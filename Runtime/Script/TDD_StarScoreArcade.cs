﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDD_StarScoreArcade : MonoBehaviour
{

    public ArcadeScore m_score;
    public GainRatio m_gainRatio;
    public GainModificatorLoseWithTime m_malus;
    public float m_minTimeBetweenAddScore = 0.1f;
    public float m_maxTimeBetweenAddScore = 10f;
    public float m_scoreToAdd = 100f;

    [Range(0f,1f)]
    public float m_pourcentMistakeDone = 0.3f;
    public float m_gainPerSuccess=0.1f;
    public float m_gainPerFailure=0.5f;
    public bool m_useFreezeWhenSucess=true;

    [Header("Debug")]
    public float m_lastAdded;
    public float m_lastAddedAffectedByGain;

  

  

    private IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(UnityEngine.Random.Range(m_minTimeBetweenAddScore, m_maxTimeBetweenAddScore));
            AddRandomScore();
        }
    }

    private void AddRandomScore()
    {
        float score = m_scoreToAdd;
        if (UnityEngine.Random.value < m_pourcentMistakeDone)
            MissScore();
        else
            AddScore(score);

    }

    private void MissScore()
    {
        RemoveGain(m_gainPerFailure);
    }

    private void RemoveGain(float gain)
    {
        m_gainRatio.RemoveGain(gain);
    }

    public void AddScore(float score) {
        m_lastAdded = score;
        m_lastAddedAffectedByGain = m_gainRatio.GetGain() * score;
        AddGain(m_gainPerSuccess);
        m_score.AddScore(m_lastAddedAffectedByGain);
        if(m_useFreezeWhenSucess)
          m_malus.FreezeGainLostWithDefaultValue();
    }
    public void ResetGain()
    {
        m_gainRatio.ResetGain();
    }


    public void AddGain(float gain)
    {
        m_gainRatio.AddGain(gain);
    }
    
}
