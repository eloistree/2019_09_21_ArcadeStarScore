﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GainModificatorClamp : MonoBehaviour
{
    public GainRatio m_gainRatio;
    public float m_minValue=0;
    public float m_maxValue=8;
    // Start is called before the first frame update
    void Start()
    {
        m_gainRatio.m_onGainChanged.AddListener(CheckValueAfterChange);
    }

    private void CheckValueAfterChange(float newValue)
    {
        if (newValue < m_minValue) {
           m_gainRatio.SetGain(m_minValue);
        }
        if (newValue > m_maxValue)
        {
           m_gainRatio.SetGain(m_maxValue);
        }
    }
    
    private void Reset()
    {
        m_gainRatio = GetComponent<GainRatio>();
    }
}
