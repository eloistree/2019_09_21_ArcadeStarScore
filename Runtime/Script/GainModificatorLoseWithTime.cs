﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainModificatorLoseWithTime : MonoBehaviour
{
    public GainRatio m_gainRatio;
    public float m_loseOverTime = 0.1f;
    public float m_cooldown=2f;
    [Header("Debug")]
    [SerializeField] float m_coundDownBeforeRemoving;

    void Start()
    {
        FreezeGainLostWithValue(m_cooldown);
    }

    private void ResetCoolDownWhenGainChanged(float arg0)
    {

        FreezeGainLostWithValue(m_cooldown);
    }

    public void FreezeGainLostWithValue(float value)
    {
        m_coundDownBeforeRemoving = value;

    }
    

    public void Update()
    {
        if (m_coundDownBeforeRemoving > 0f) {
            m_coundDownBeforeRemoving -= Time.deltaTime;
            return;
        }
        if (m_gainRatio.GetGain() <= 0f)
            return;

        m_gainRatio.RemoveGain(m_loseOverTime * Time.deltaTime);
    }
    private void Reset()
    {
        m_gainRatio = GetComponent<GainRatio>();
    }

    public void FreezeGainLostWithDefaultValue()
    {
        FreezeGainLostWithValue(m_cooldown);
    }
}
