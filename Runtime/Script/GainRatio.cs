﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GainRatio : MonoBehaviour
{
    public bool m_allowNegativeGain;

    [Header("Event")]
    public NewGainStateChangeEvent m_onGainChanged;


    [Header("Debug")]
    public float m_gainRatioState;


    private void Awake()
    {
        NotifyChangeOfRatio();
    }

    public void AddGain(float ratioInPourcent)
    {
        SetGain(GetGain() + ratioInPourcent);
    }
    public void RemoveGain(float ratioInPourcent)
    {
        SetGain( GetGain()- ratioInPourcent);
    }

    public void SetGain(float gainInPourcent)
    {
        if (!m_allowNegativeGain && gainInPourcent < 0f)
            gainInPourcent = 0;
        bool isNewValue = gainInPourcent != m_gainRatioState; 

        m_gainRatioState = gainInPourcent;

        if (isNewValue )
            NotifyChangeOfRatio();

    }

    private void NotifyChangeOfRatio()
    {
        m_onGainChanged.Invoke(m_gainRatioState);
    }
    public void ResetGain( )
    {
        SetGain(0);
    }
    public float GetGain()
    {
        return m_gainRatioState;
    }

    [System.Serializable]
    public class NewGainStateChangeEvent : UnityEvent<float> { };

  

    //public void OnValidate()
    //{
    //    SetGain(m_gainRatioState);
    //    NotifyChangeOfRatio();
    //}
}
