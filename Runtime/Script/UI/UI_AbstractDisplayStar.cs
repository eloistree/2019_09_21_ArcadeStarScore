﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UI_AbstractDisplayStar : MonoBehaviour
{
    [SerializeField] protected float m_positiveRatio;
    public void SetStarByUsingRatio(float ratio) {
        if (ratio < 0f)
            ratio = 0;
        float oldValue = m_positiveRatio;
        float newValue = ratio;
       
        m_positiveRatio = ratio;

        if (oldValue != newValue)
            OnRatioChanged(oldValue, newValue);
    }
    public float GetCurrentRatio() { return m_positiveRatio; }

    protected abstract void OnRatioSetted(float newValue);
    protected abstract void OnRatioChanged(float oldValue, float newValue);
}
