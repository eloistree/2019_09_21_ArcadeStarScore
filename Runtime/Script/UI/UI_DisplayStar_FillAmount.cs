﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DisplayStar_FillAmount : UI_AbstractDisplayStar
{

    public Image m_forgroundImageToAffect;
    public Image m_epicBonusStarBorder;
    [Range(0.1f, 3)]
    public float m_lerpPower=1;
    public float m_bonusBorderAmountStart = 0.99f;
    public float m_bonusBorderAmountEnd = 3;

    [Header("Debug")]
    [SerializeField] float m_currentValue;
    [SerializeField] float m_wantedValue;

    public void Update()
    {
        //if (Mathf.Abs(m_currentValue - m_wantedValue) < 0.01f) {
        //    return;
        //}
        float newFill = Mathf.Lerp(m_currentValue, m_wantedValue, Time.deltaTime * m_lerpPower);
        m_forgroundImageToAffect.fillAmount = newFill;
        m_currentValue = newFill;
        m_epicBonusStarBorder.enabled = m_currentValue >= m_bonusBorderAmountStart;
        if (m_bonusBorderAmountEnd - m_bonusBorderAmountStart != 0) {
            float bonusFill = (m_currentValue - m_bonusBorderAmountStart) / ( m_bonusBorderAmountEnd- m_bonusBorderAmountStart) ;
            m_epicBonusStarBorder.fillAmount = bonusFill;
        }
    }


    protected override void OnRatioChanged(float oldValue, float newValue)
    {
        RefreshWantedStateOfStarsDisplay(newValue);
    }

    protected override void OnRatioSetted(float newValue)
    {
        RefreshWantedStateOfStarsDisplay(newValue);
    }

    public void RefreshWantedStateOfStarsDisplay(float wantedFillAmount) {

        m_wantedValue = wantedFillAmount;
    }
}
