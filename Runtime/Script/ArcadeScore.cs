﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// QUICK DIRTY CODE CREATE YOUR OWN TO HAVE  A CLEAN ONE
public class ArcadeScore : MonoBehaviour
{
    public float m_score;
    public Text m_scoreDisplayerDebugger;
    public string m_formatScore = "{0:00000}";

    public void SetScore(float score)
    {
        m_score = score;
        if (m_scoreDisplayerDebugger != null)
            m_scoreDisplayerDebugger.text = string.Format(m_formatScore, score );
    }
    public float GetScore()
    {
        return m_score;
    }

    public  void AddScore(float score)
    {
        SetScore(GetScore() + score);
    }
}
