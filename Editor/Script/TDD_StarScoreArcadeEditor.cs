﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TDD_StarScoreArcade))]
public class TDD_StarScoreArcadeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TDD_StarScoreArcade myScript = (TDD_StarScoreArcade)target;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add score"))
        {
            myScript.AddScore(200f);
        }
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add 0.1 Gain"))
        {
            myScript.AddGain(0.1f);
        }
        if (GUILayout.Button("Add 1 Gain"))
        {
            myScript.AddGain(1f);
        }
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Reset Gain"))
        {
            myScript.ResetGain();
        }
    }
}
